from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Laila Saffanah' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 11, 2) #TODO Implement this, format (Year, Month, Date)
npm = 1706043531 # TODO Implement this
major = 'Computer Information System'
univ = 'Universitas Indonesia'
description = 'Animation concept enthusiast, mainly in steampunk concept'

fivi_name = 'Fivi Melinda'
fivi_birth = date(1999,7,13)
fivi_npm = 1706984594
fivi_major = 'Computer Information System'
fivi_univ = 'Universitas Indonesia'
fivi_description = 'Pink enthusiast'

caca_name = 'Salsabila Maurizka'
caca_birth = date(2000,4,14)
caca_npm = 1706043986
caca_major = 'Computer Information System'
caca_univ = 'Universitas Indonesia'
caca_description = 'I love fencing'

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'major':major, 'univ':univ,'age': calculate_age(birth_date.year), 'npm': npm, 'description':description,
                'fivi_name': fivi_name, 'fivi_major':fivi_major, 'fivi_univ':fivi_univ,'fivi_age': calculate_age(fivi_birth.year), 'fivi_npm': fivi_npm, 'fivi_description':fivi_description,
                'caca_name': caca_name, 'caca_major':caca_major, 'caca_univ':caca_univ,'caca_age': calculate_age(caca_birth.year), 'caca_npm': fivi_npm, 'caca_description':caca_description}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
